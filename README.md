# ids721-week7-mini

The main goal for this project is to ingest data into Vector database, perform queries and aggregations, and visualize the output.



## Getting Start
1. Run `cargo new week7-mini` to create a new Rust project.

2. Add the required dependencies to `Cargo.toml` file.

3. start the qdrant service by running:
```bash
docker run -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
![](1.png)

4. In `main.rs`, I implement the data ingestion and query functionality. I ran a query that was executed to search a place closest to the given vector. 

## Visualization
5. After ingesting the data, we can run `cargo run` to run query: \
![](2.png)

