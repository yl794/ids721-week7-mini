use anyhow::{Result};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, PointStruct, SearchPoints, Condition, Filter, VectorParams, VectorsConfig
};
use qdrant_client::qdrant::vectors_config::Config;
use serde_json::json;
use std::convert::TryInto;

#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "test_collection";
    create_collection(&client, collection_name).await?;
    ingest_vectors(&client, collection_name).await?;
    query_vectors(&client, collection_name).await?;

    Ok(())
}

async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}

async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Delete collection if it already exists
    let _ = client.delete_collection(collection_name).await;

    // Create collection with desired configuration
    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Euclid.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}

async fn ingest_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Create 5 vectors
    let points = vec![
        PointStruct::new(
            1,
            vec![0.05, 0.61, 0.76, 0.74],
            json!({"city": "city1"}).try_into().unwrap(),
        ),
        PointStruct::new(
            2,
            vec![0.19, 0.81, 0.75, 0.11],
            json!({"city": "city2"}).try_into().unwrap(),
        ),
        PointStruct::new(
            3,
            vec![0.12, 0.72, 0.69, 0.33],
            json!({"city": "city3"}).try_into().unwrap(),
        ),
        PointStruct::new(
            4,
            vec![0.42, 0.21, 0.54, 0.85],
            json!({"city": "city4"}).try_into().unwrap(),
        ),
        PointStruct::new(
            5,
            vec![0.91, 0.62, 0.34, 0.25],
            json!({"city": "city5"}).try_into().unwrap(),
        ),
    ];

    // Upsert points into the collection
    client.upsert_points_blocking(collection_name, None, points, None).await.unwrap();

    Ok(())
}

async fn query_vectors(client: &QdrantClient, collection_name: &str) -> Result<()> {
    // Run a query on the data
    let search_result = client
        .search_points(&SearchPoints {
            collection_name: collection_name.into(),
            vector: vec![0.1, 0.2, 0.3, 0.4],
            limit: 1,
            with_payload: Some(true.into()),
            ..Default::default()
        })
        .await
        .unwrap();

    // Print header
    println!("Closest:");

    // Iterate over each found point
    for (index, point) in search_result.result.iter().enumerate() {
        // Format the payload nicely
        let formatted_payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        // Print each point's information
        println!("Point {}: ", index + 1);
        println!(" - Payload: {}", formatted_payload);
        println!(" - Score: {}", point.score); // Print the score instead of distance
        println!(); // Add an empty line for better readability
    }

    Ok(())
}